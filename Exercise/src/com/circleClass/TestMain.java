package com.circleClass;

public class TestMain {
    public static void main(String[] args) {
        Circle c1 = new Circle(1.1,"red");
        System.out.println(c1);
        Circle c2 = new Circle();
        System.out.println(c2);

        c1.setRadius(2.3);
        System.out.println(c1);
        System.out.println("radius is : " + c1.getRadius());

        System.out.println("The area is :"+c1.getArea());
        System.out.println("circumference is : "+ c1.getCircumference());
    }
}
