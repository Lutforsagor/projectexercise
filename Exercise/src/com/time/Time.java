package com.time;

public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour,int minute,int second){
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    public int getHour(){
        return hour;
    }
    public int getMinute(){
        return minute;
    }
    public int getSecond(){
        return second;
    }
    public void setHour(int hour){
        this.hour = hour;
    }
    public void setMinute(int minute){
        this.minute = minute;
    }
    public void setSecond(int second){
        this.second = second;
    }
    public void setTime(int hour, int minute, int second){
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
    public String toString(){
        String h = String.format("%02d",hour);
        String m = String.format("%02d",minute);
        String s = String.format("%02d",second);
        return "" + h + ":" + m + ":" + s;
    }
    public Time nextSecond(){
        if(this.second+1 == 60){
            this.second = 00;
        }
        else{
            this.second = this.second + 1;
        }

        if(this.minute+1 == 60){
            this.minute = 00;
        }
        else{
            this.minute = this.minute + 1;
        }

        if(this.hour+1 == 24){
            this.hour = 00;
        }
        else{
            this.hour = this.hour + 1;
        }
        return this;
    }

    public Time previousSecond(){
        if(this.second == 00){
            this.second = 59;
        }
        else{
            this.second = this.second - 1;
        }
        if(this.minute == 00){
            this.minute = 59;
        }
        else{
            this.minute = this.minute - 1;
        }
        if(this.hour == 00){
            this.hour = 23;
        }
        else{
            this.hour = this.hour - 1;
        }
        return this;
    }
}
