package com.time;

public class TestMain {
    public static void main(String[] args) {
        Time t1 = new Time(4, 6, 15);
        System.out.println(t1);

        t1.setHour(5);
        t1.setMinute(8);
        t1.setSecond(6);
        System.out.println(t1);
        System.out.println("Hour: " + t1.getHour());
        System.out.println("Minute: " + t1.getMinute());
        System.out.println("Second: " + t1.getSecond());

        t1.setTime(22, 49, 14);
        System.out.println(t1);

        System.out.println(t1.nextSecond());
        System.out.println(t1.nextSecond().nextSecond());

        System.out.println(t1.previousSecond());
        System.out.println(t1.previousSecond().previousSecond());
    }
}
