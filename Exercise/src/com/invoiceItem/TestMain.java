package com.invoiceItem;

public class TestMain {
    public static void main(String[] args) {
        InvoiceItem inv1 = new InvoiceItem("A1", "Pen Blue", 10, 100);
        System.out.println(inv1);
        inv1.setQty(15);
        inv1.setUnitPrice(200);
        System.out.println(inv1);

        System.out.println("id is: " + inv1.getID());
        System.out.println("desc is: " + inv1.getDesc());
        System.out.println("qty is: " + inv1.getQty());
        System.out.println("unitPrice: " + inv1.getUnitPrice());

        System.out.println("The Total is: " + inv1.getTotal());
        }
    }